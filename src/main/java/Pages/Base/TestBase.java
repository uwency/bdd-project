package Pages.Base;

import Pages.util.TestUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {
    public static WebDriver driver;
    public static Properties prop;

    public TestBase() {
        try {
            prop = new Properties();
            FileInputStream file = new FileInputStream("src/main/java/Pages/config/config.properties");
            prop.load(file);

        }catch (IOException e) {
            e.getMessage();
        }
    }

    public static void initialize(){
        String browser = prop.getProperty("browser");
        if(browser.equals("chrome"))
        {
            System.setProperty("webdriver.chrome.driver","driver/chromedriver.exe");
            driver = new ChromeDriver();
        }
        if(browser.equals("firefox"))
        {
            System.setProperty("webdriver.gecko.driver","driver/geckodriver.exe");
            driver = new FirefoxDriver();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(TestUtil.IMPLICIT_WAIT,TimeUnit.SECONDS);
        driver.get(prop.getProperty("url"));
    }
}
