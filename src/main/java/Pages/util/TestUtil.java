package Pages.util;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class TestUtil {
    public static int PAGE_LOAD_TIMEOUT = 100;
    public static int IMPLICIT_WAIT = 100;

    public void takeSnapShot(WebDriver driver){

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("F:\\Exercise Automation\\CucumberTestNGPOM\\Screenshots\\ThankYou.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
