package StepsDefination;

import Pages.Base.TestBase;
import Pages.Page.ShoppingCart;
import cucumber.api.java.en.Then;

public class ShoppingCartPage extends TestBase {
    ShoppingCart shoppingCart;

    @Then("^user click the checkout Button$")
    public void user_click_the_checkout_Button(){
        shoppingCart = new ShoppingCart();
        shoppingCart.clickCheckOutButton();
    }


}
