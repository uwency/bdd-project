package StepsDefination;

import Pages.Base.TestBase;
import Pages.Page.Shopping;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.it.Ma;
import java.util.Map;

public class ShoppingPage extends TestBase {
    Shopping shopping;

    @Then("^user select item$")
    public void user_select_item(DataTable credentials) {
        shopping = new Shopping();
        for (Map<String, String> storeItems : credentials.asMaps(String.class, String.class)) {
            shopping.AddtoCartItemviaProductView(storeItems.get("Items"));
        }
    }
    @Then("^user click and view the shopping Cart Items$")
    public void user_click_and_view_the_shopping_Cart_Items(){
        shopping.clickshoppingCartButton();

        }

}
