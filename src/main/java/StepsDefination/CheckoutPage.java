package StepsDefination;

import Pages.Base.TestBase;
import Pages.Page.CheckOut;
import cucumber.api.java.en.Then;

public class CheckoutPage extends TestBase {
    CheckOut checkOut = new CheckOut();

    @Then("^user click finish button$")
    public void user_click_finish_button(){
        checkOut.clickFinishButton();
    }
}

