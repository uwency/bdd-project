package StepsDefination;

import Pages.Page.ThankYou;
import cucumber.api.java.en.Then;
import org.testng.Assert;

public class ThankYouPage {
    ThankYou thankYou = new ThankYou();


    @Then("^user verify that the purchase is successful$")
    public void user_verify_that_the_purchase_is_successful(){
        Assert.assertEquals(thankYou.verifyFinishText(),"Finish");
        Assert.assertTrue(thankYou.checkThankYouLogo());
        Assert.assertEquals(thankYou.thankYouOrderText(),"THANK YOU FOR YOUR ORDER");
    }
}
