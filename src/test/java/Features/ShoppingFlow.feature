Feature: Shopping Page Validation

  Scenario Outline: To verify that user is able to add item on cart and checkout
    Given Open Browser
    Then User enter valid "<username>" and "<password>"
    Then User click login button
    Then verify that user is able to login successfully
    Then user select item
    |Items|
    |Sauce Labs Fleece Jacket |

    Then user click and view the shopping Cart Items
    Then user click the checkout Button
    Then user enter "<FirstName>" and "<LastName>" and "<ZipCode>"
    Then user click the continue button
    Then user click finish button
    Then user verify that the purchase is successful

    @ShopAndPay
    Examples:
      | username        | password     | FirstName | LastName | ZipCode |
      | standard_user   | secret_sauce | Uriel     | Wenceslao| 20039   |
