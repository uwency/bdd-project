Feature: Login Page Validation
  Scenario Outline: This test is to verify that all login valid details is handled correctly
    Given Open Browser
    Then User enter valid "<username>" and "<password>"
    Then User click login button
    Then verify that user is able to login successfully

    @Regression
    Examples:
      | username        | password     |
      | standard_user   | secret_sauce |
      | problem_user    | secret_sauce |
      | performance_glitch_user | secret_sauce |

    @Smoke
    Examples:
      | username        | password     |
      | standard_user   | secret_sauce |



  Scenario Outline: This test is to verify that blocked user is not allowed
    Given Open Browser
    Then user enter blocked "<username>" and "<password>"
    @Smoke @Regression
    Examples:
      | username        | password     |
      | locked_out_user | secret_sauce |
